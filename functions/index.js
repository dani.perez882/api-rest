const functions = require('firebase-functions');

const express = require('express');


const mongoose = require('mongoose');
const configMongoose = { useNewUrlParser: true }
const { username, password } = functions.config().mongo;

const mongoUri = `mongodb://${username}:${password}@ds159574.mlab.com:59574/pruebas`
mongoose.connect(mongoUri, configMongoose);



const cors = require('cors');

const app = express();

const Pets = require('./pets.js');
const createServer = () => {

    app.use(cors({ origin: true }))
    app.get('/pets', async(req, res) => {
        const resultpets = await Pets.find({}).exec();
        res.send(resultpets);

    });


    app.post('/pets', async(req, res) => {

        const datos = req.body
        const newpet = new Pets(datos)
        await newpet.save()
        res.sendStatus(204);

    });

    app.get('/pets/:id/daralta', async(req, res) => {

        const { id } = req.params;
        await Pets.deleteOne({ _id: id }).exec()
        res.sendStatus(204);

    });
    return app;
}